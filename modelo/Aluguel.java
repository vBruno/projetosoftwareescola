package modelo;
import java.util.Date;

public class Aluguel{
    private Date locacao;
    private String devolucao;
    public Aluno aluno = new Aluno();
    public Livro livro = new Livro();

    public Date getLocacao() {
        return locacao;
    }

    public void setLocacao(Date locacao) {
        this.locacao = locacao;
    }

    public String getDevolucao() {
        return devolucao;
    }

    public void setDevolucao(String devolucao) {
        this.devolucao = devolucao;
    }
     
        }
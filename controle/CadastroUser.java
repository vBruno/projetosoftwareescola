/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controle;

/**
 *
 * @author ulybr, tarcisio
 */
import modelo.User; //import da classe modelo
import java.sql.SQLException; //import das excessões SQL
import java.sql.PreparedStatement; //import para inserseção dos dados
import java.sql.ResultSet;

public class CadastroUser {
    public boolean Cadastro(User user){
        Conexao con = new Conexao();
//        User user = new User();
      
        try{
            con.abrirConexao();
            PreparedStatement ps = con.getCon().prepareStatement("INSERT into usuario (nome, senha, email) VALUES(?, ?, ?);");
            ps.setString(1, user.getNome());
            ps.setString(2, user.getSenha());
            ps.setString(3, user.getEmail());
            if(!ps.execute()){
                return true;
            }else{
                return false;
            }
            
//            return !ps.execute();
        } catch(SQLException e){
            System.out.println("[-] Erro ao inserir usuário");
            System.out.println(e.getMessage());
            return false;
        } finally{
            con.fecharConexao();
        }
        
        
    }
    public boolean verificaUser(String nome){
        Conexao con = new Conexao();
        try{
            con.abrirConexao();
            PreparedStatement ps = con.getCon().prepareStatement("SELECT * FROM usuario WHERE nome=?;");
            ps.setString(1,nome);
            ResultSet rs = ps.executeQuery();
            if(!rs.next()){
                return true;
            }else{
                return false;
            }
        }catch(SQLException e){
            System.out.println("[-] Erro ao conferir existência de nome de usuário");
            System.out.println(e.getMessage());
            return false;
        }finally{
            con.fecharConexao();
        }
    }

}



/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controle;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import controle.Conexao;
import java.text.SimpleDateFormat;
import java.util.Date;
import visual.Expirados;
/**
 *
 * @author bruno
 */
public class AluguelC {

    public boolean deletarAluguel(int codigo){
        Conexao con = new Conexao();
        try{
            con.abrirConexao();
            PreparedStatement ps = con.getCon().prepareStatement("DELETE FROM aluguel WHERE cod=?");
            ps.setInt(1, codigo);
            if(!ps.execute()){
                return true;
            }else{
                return false;
            }
        }catch(SQLException e){
            System.out.println("Erro ao Excluir elemento");
            System.out.println(e.getMessage());
        }finally{
            con.fecharConexao();
        }
        return false;
    }
}

package visual;

//@author lavigne

public class TelaInicial extends javax.swing.JFrame {
    //Instancias das classes jInternalFrame
    AdicionarAluguel AddAlug = new AdicionarAluguel();
    AdicionarLivro AddLivr = new AdicionarLivro();
    Alugueis Alug = new Alugueis();
    Expirados Expird = new Expirados();
    Perfil Perfil = new Perfil();
    //Fim das instancias
    
    //Método Construtor;
    public TelaInicial() {
        initComponents();
        //Deixa a tela maximizada;
        setExtendedState(TelaInicial.MAXIMIZED_BOTH); 
    }
    //Fim do  Método Construtor;
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jpnFundo = new javax.swing.JPanel();
        jpnMenu = new javax.swing.JPanel();
        btnPerfil = new javax.swing.JButton();
        btnExpird = new javax.swing.JButton();
        btnAlugueis = new javax.swing.JButton();
        btnAddAlug = new javax.swing.JButton();
        btnAddLivro = new javax.swing.JButton();
        jpnConteiner = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Távola Redonda");
        setResizable(false);

        jpnFundo.setBackground(new java.awt.Color(255, 255, 255));

        jpnMenu.setBackground(new java.awt.Color(90, 207, 184));

        btnPerfil.setBackground(new java.awt.Color(90, 207, 184));
        btnPerfil.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imgs/IconPerfil.png"))); // NOI18N
        btnPerfil.setBorder(null);
        btnPerfil.setBorderPainted(false);
        btnPerfil.setContentAreaFilled(false);
        btnPerfil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPerfilActionPerformed(evt);
            }
        });

        btnExpird.setBackground(new java.awt.Color(90, 207, 184));
        btnExpird.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imgs/IconExpirados.png"))); // NOI18N
        btnExpird.setBorder(null);
        btnExpird.setBorderPainted(false);
        btnExpird.setContentAreaFilled(false);
        btnExpird.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExpirdActionPerformed(evt);
            }
        });

        btnAlugueis.setBackground(new java.awt.Color(90, 207, 184));
        btnAlugueis.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imgs/IconAlugueis.png"))); // NOI18N
        btnAlugueis.setBorder(null);
        btnAlugueis.setBorderPainted(false);
        btnAlugueis.setContentAreaFilled(false);
        btnAlugueis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAlugueisActionPerformed(evt);
            }
        });

        btnAddAlug.setBackground(new java.awt.Color(90, 207, 184));
        btnAddAlug.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imgs/IconAdicionar.png"))); // NOI18N
        btnAddAlug.setBorder(null);
        btnAddAlug.setBorderPainted(false);
        btnAddAlug.setContentAreaFilled(false);
        btnAddAlug.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddAlugActionPerformed(evt);
            }
        });

        btnAddLivro.setBackground(new java.awt.Color(90, 207, 184));
        btnAddLivro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imgs/IconAddLivro.png"))); // NOI18N
        btnAddLivro.setBorder(null);
        btnAddLivro.setBorderPainted(false);
        btnAddLivro.setContentAreaFilled(false);
        btnAddLivro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddLivroActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jpnMenuLayout = new javax.swing.GroupLayout(jpnMenu);
        jpnMenu.setLayout(jpnMenuLayout);
        jpnMenuLayout.setHorizontalGroup(
            jpnMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnMenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpnMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpnMenuLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jpnMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnAddAlug, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnPerfil, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addGroup(jpnMenuLayout.createSequentialGroup()
                        .addGroup(jpnMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnAlugueis)
                            .addComponent(btnExpird)
                            .addComponent(btnAddLivro))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jpnMenuLayout.setVerticalGroup(
            jpnMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpnMenuLayout.createSequentialGroup()
                .addGap(56, 56, 56)
                .addComponent(btnAddAlug, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(67, 67, 67)
                .addComponent(btnAddLivro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(69, 69, 69)
                .addComponent(btnAlugueis, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(76, 76, 76)
                .addComponent(btnExpird, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(75, 75, 75)
                .addComponent(btnPerfil, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(50, 50, 50))
        );

        btnPerfil.getAccessibleContext().setAccessibleName("Acessar seu perfil");
        btnPerfil.getAccessibleContext().setAccessibleDescription("Aqui você pode mudar seu nome de usuário e senha");
        btnExpird.getAccessibleContext().setAccessibleName("Acessar alugueis de livros expirados");
        btnAlugueis.getAccessibleContext().setAccessibleName("Acessar alugueis de livro já salvos");
        btnAddAlug.getAccessibleContext().setAccessibleName("Adicionar novo aluguel de livro");
        btnAddLivro.getAccessibleContext().setAccessibleName("Adicionar cadastro de livro ");
        btnAddLivro.getAccessibleContext().setAccessibleDescription("isso serve para que os livros já sejam pré carregados");

        jpnConteiner.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jpnConteinerLayout = new javax.swing.GroupLayout(jpnConteiner);
        jpnConteiner.setLayout(jpnConteinerLayout);
        jpnConteinerLayout.setHorizontalGroup(
            jpnConteinerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 955, Short.MAX_VALUE)
        );
        jpnConteinerLayout.setVerticalGroup(
            jpnConteinerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jpnFundoLayout = new javax.swing.GroupLayout(jpnFundo);
        jpnFundo.setLayout(jpnFundoLayout);
        jpnFundoLayout.setHorizontalGroup(
            jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnFundoLayout.createSequentialGroup()
                .addComponent(jpnMenu, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jpnConteiner, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jpnFundoLayout.setVerticalGroup(
            jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpnMenu, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jpnFundoLayout.createSequentialGroup()
                .addComponent(jpnConteiner, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpnFundo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpnFundo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    //Enventos de Click dos Botões do menu;
    private void btnExpirdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExpirdActionPerformed
        jpnConteiner.add(Expird);
        AddAlug.setVisible(false);
        AddLivr.setVisible(false);
        Alug.setVisible(false);
        Expird.setVisible(true);
        Perfil.setVisible(false);
    }//GEN-LAST:event_btnExpirdActionPerformed

    private void btnAddAlugActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddAlugActionPerformed
        //Adiciona a tela de Adicionar Aluguel ao painel quando o Botão de Adicionar é clicado;
        jpnConteiner.add(AddAlug);
        AddAlug.setVisible(true);
        AddLivr.setVisible(false);
        Alug.setVisible(false);
        Expird.setVisible(false);
        Perfil.setVisible(false);
    }//GEN-LAST:event_btnAddAlugActionPerformed

    private void btnAlugueisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAlugueisActionPerformed
        jpnConteiner.add(Alug);
        AddAlug.setVisible(false);
        AddLivr.setVisible(false);
        Alug.setVisible(true);
        Expird.setVisible(false);
        Perfil.setVisible(false);
    }//GEN-LAST:event_btnAlugueisActionPerformed

    private void btnPerfilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPerfilActionPerformed
        jpnConteiner.add(Perfil);
        AddAlug.setVisible(false);
        AddLivr.setVisible(false);
        Alug.setVisible(false);
        Expird.setVisible(false);
        Perfil.setVisible(true);
    }//GEN-LAST:event_btnPerfilActionPerformed

    private void btnAddLivroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddLivroActionPerformed
        jpnConteiner.add(AddLivr);
        AddLivr.setVisible(true);
        AddAlug.setVisible(false);
        Alug.setVisible(false);
        Expird.setVisible(false);
        Perfil.setVisible(false);
    }//GEN-LAST:event_btnAddLivroActionPerformed
    //Fim dos eventos de botões;
    
    
    /*
       Método  main temporário (será apagado quando toda a aplicação estiver montada, pois é necessário que haja um
       arquivo execultável para fazer  testes);
    */
    //Fim do método main;
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddAlug;
    private javax.swing.JButton btnAddLivro;
    private javax.swing.JButton btnAlugueis;
    private javax.swing.JButton btnExpird;
    private javax.swing.JButton btnPerfil;
    public javax.swing.JPanel jpnConteiner;
    private javax.swing.JPanel jpnFundo;
    private javax.swing.JPanel jpnMenu;
    // End of variables declaration//GEN-END:variables
}

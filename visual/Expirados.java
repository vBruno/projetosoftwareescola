package visual;
//@author lavigne
import controle.AluguelC;
import java.text.SimpleDateFormat;
import java.util.Date;
import controle.Conexao;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.table.DefaultTableModel;
 
public class Expirados extends javax.swing.JInternalFrame {

    //Método Construtor
    public Expirados(){
        initComponents();
        this.Expirados(date1);
    }
    //Fim do Método Construtor
    Date date = new Date();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    String formattedDate = simpleDateFormat.format(date);
    java.sql.Date date1 = java.sql.Date.valueOf(formattedDate);
    
    public void Expirados(java.sql.Date date1){
        Conexao con = new Conexao();
        try{
            con.abrirConexao();
            PreparedStatement ps = con.getCon().prepareStatement("select aluno.matricula, aluno.nome AS aluno, livro.nome AS livro, aluguel.dataDevolucao, aluguel.cod from aluguel inner join aluno on aluno.matricula=aluguel.matricula inner join livro on livro.id=aluguel.id and aluguel.dataDevolucao<=?;");
            ps.setDate(1, date1);
            ResultSet rs = ps.executeQuery();
            
            DefaultTableModel model = (DefaultTableModel) tabelaExpirados.getModel();
            model.setNumRows(0);
            
            while(rs.next()){
                model.addRow(new Object[] {
                    rs.getInt("matricula"),
                    rs.getString("aluno"),
                    rs.getString("livro"),
                    rs.getString("dataDevolucao"),
                    rs.getInt("cod")
                });
            }       
        }catch(SQLException e){
            System.out.println("Deu Erro");
            System.out.println(e.getMessage());
        }finally{
            con.fecharConexao();
        }  
    }
    
//    public void Expirados(java.sql.Date date1){
//        Conexao con = new Conexao();
//        try{
//            con.abrirConexao();            
//            PreparedStatement ps = con.getCon().prepareStatement("select aluno.matricula, aluno.nome AS aluno, livro.nome AS livro, aluguel.dataDevolucao from aluguel inner join aluno on aluno.matricula=aluguel.matricula inner join livro on livro.id=aluguel.id and aluguel.dataDevolucao<=?;");
//            ps.setDate(1, date1);
//            ResultSet rs = ps.executeQuery();
//            DefaultTableModel model = (DefaultTableModel) tabelaExpirados.getModel();
//            model.setNumRows(0);
//            while(rs.next()){
//                model.addRow(new Object[]{
//                    rs.getInt("matricula"),
//                    rs.getString("aluno"),
//                    rs.getString("livro"),
//                    rs.getString("dataDevolucao")
//                });
//            }
//        }catch(SQLException e){
//            System.out.println("Erro");
//            System.out.println(e.getMessage());
//        }finally{
//            con.fecharConexao();
//        }
//    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaExpirados = new javax.swing.JTable();
        btnApagarRow = new javax.swing.JButton();
        jtextCod = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();

        tabelaExpirados.setBackground(new java.awt.Color(90, 207, 184));
        tabelaExpirados.setFont(new java.awt.Font("Fira Sans", 0, 15)); // NOI18N
        tabelaExpirados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Matricula", "Nome Aluno", "Nome Livro", "Data Expiração", "Código Aluguel"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tabelaExpirados);

        btnApagarRow.setText("Deletar");
        btnApagarRow.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnApagarRowActionPerformed(evt);
            }
        });

        jLabel1.setText("Digite o código para excluir o aluguel");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 745, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jtextCod, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(92, 92, 92)
                        .addComponent(btnApagarRow)))
                .addGap(0, 33, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 491, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(51, 51, 51)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jtextCod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnApagarRow)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        setBounds(125, 50, 1043, 539);
    }// </editor-fold>//GEN-END:initComponents

    private void btnApagarRowActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnApagarRowActionPerformed
//        Conexao con = new Conexao();
//        try{
//            con.abrirConexao();
//            PreparedStatement ps = con.getCon().prepareStatement("DELETE FROM aluguel WHERE cod=?");
//            int codigo = Integer.parseInt(jtextCod.getText().trim());
//            ps.setInt(1, codigo);
//            if(!ps.execute()){
//                this.Expirados(date1);
//                jtextCod.setText("");
//            }
//        }catch(SQLException e){
//            System.out.println("Erro ao Excluir elemento");
//            System.out.println(e.getMessage());
//        }finally{
//            con.fecharConexao();
//        }     
        AluguelC aluguel = new AluguelC();
        int codigo = Integer.parseInt(jtextCod.getText().trim());
        if(aluguel.deletarAluguel(codigo)){
            this.Expirados(date1);
            jtextCod.setText("");
        }
    }//GEN-LAST:event_btnApagarRowActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnApagarRow;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jtextCod;
    private javax.swing.JTable tabelaExpirados;
    // End of variables declaration//GEN-END:variables
}

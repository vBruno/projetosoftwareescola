
package visual;

// @author lavigne
 
public class AdicionarAluguel extends javax.swing.JInternalFrame {
    
    //Método Construtor;
    public AdicionarAluguel() {
        initComponents();
    }
    //Fim do Método Construtor;

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jpnFundo = new javax.swing.JPanel();
        jlblNomeAluno = new javax.swing.JLabel();
        jlblTurma = new javax.swing.JLabel();
        jlblTituloLivro = new javax.swing.JLabel();
        jlblDataAluguel = new javax.swing.JLabel();
        jlblDataDevolucao = new javax.swing.JLabel();
        jtxtfNomeAluno = new javax.swing.JTextField();
        jtxtfTurma = new javax.swing.JTextField();
        jtxtfTituloLivro = new javax.swing.JTextField();
        jformattedtxtfDataAluguel = new javax.swing.JFormattedTextField();
        jformattedtxtfDataDevolucao = new javax.swing.JFormattedTextField();
        btnSalvar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        btnFechar = new javax.swing.JButton();
        jpnBorda = new javax.swing.JPanel();
        jlblTitulo = new javax.swing.JLabel();
        jtxtfEmailAluno = new javax.swing.JTextField();
        jlblEmailAluno = new javax.swing.JLabel();
        btnAddLivro = new javax.swing.JButton();

        setBorder(null);
        setTitle("Adicionar Aluguel de Livro");
        setFocusable(false);
        setVisible(true);
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                formMouseClicked(evt);
            }
        });

        jpnFundo.setBackground(new java.awt.Color(239, 253, 251));

        jlblNomeAluno.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
        jlblNomeAluno.setText("Nome do Aluno:");

        jlblTurma.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
        jlblTurma.setText("Turma:");

        jlblTituloLivro.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
        jlblTituloLivro.setText("Título do Livro:");

        jlblDataAluguel.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
        jlblDataAluguel.setText("Data de Aluguel:");

        jlblDataDevolucao.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
        jlblDataDevolucao.setText("Data de Devolução:");

        jtxtfNomeAluno.setBackground(new java.awt.Color(204, 204, 204));
        jtxtfNomeAluno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtxtfNomeAlunoActionPerformed(evt);
            }
        });

        jtxtfTurma.setBackground(new java.awt.Color(204, 204, 204));

        jtxtfTituloLivro.setBackground(new java.awt.Color(204, 204, 204));
        jtxtfTituloLivro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtxtfTituloLivroActionPerformed(evt);
            }
        });

        jformattedtxtfDataAluguel.setBackground(new java.awt.Color(204, 204, 204));

        jformattedtxtfDataDevolucao.setBackground(new java.awt.Color(204, 204, 204));

        btnSalvar.setBackground(new java.awt.Color(90, 207, 184));
        btnSalvar.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
        btnSalvar.setText("Salvar");
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        btnCancelar.setBackground(new java.awt.Color(90, 207, 184));
        btnCancelar.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnEditar.setBackground(new java.awt.Color(90, 207, 184));
        btnEditar.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
        btnEditar.setText("Editar");
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });

        btnFechar.setBackground(new java.awt.Color(90, 207, 184));
        btnFechar.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
        btnFechar.setText("Fechar");
        btnFechar.setToolTipText("");
        btnFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFecharActionPerformed(evt);
            }
        });

        jpnBorda.setBackground(new java.awt.Color(90, 207, 184));

        javax.swing.GroupLayout jpnBordaLayout = new javax.swing.GroupLayout(jpnBorda);
        jpnBorda.setLayout(jpnBordaLayout);
        jpnBordaLayout.setHorizontalGroup(
            jpnBordaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 31, Short.MAX_VALUE)
        );
        jpnBordaLayout.setVerticalGroup(
            jpnBordaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jlblTitulo.setFont(new java.awt.Font("Monospaced", 1, 24)); // NOI18N
        jlblTitulo.setText("Informações do Aluguel de Livro:");

        jtxtfEmailAluno.setBackground(new java.awt.Color(204, 204, 204));
        jtxtfEmailAluno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtxtfEmailAlunoActionPerformed(evt);
            }
        });

        jlblEmailAluno.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
        jlblEmailAluno.setText("Email do Aluno:");

        btnAddLivro.setBackground(new java.awt.Color(90, 207, 184));
        btnAddLivro.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
        btnAddLivro.setText("Adicionar");
        btnAddLivro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddLivroActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jpnFundoLayout = new javax.swing.GroupLayout(jpnFundo);
        jpnFundo.setLayout(jpnFundoLayout);
        jpnFundoLayout.setHorizontalGroup(
            jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpnFundoLayout.createSequentialGroup()
                .addComponent(jpnBorda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpnFundoLayout.createSequentialGroup()
                        .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jpnFundoLayout.createSequentialGroup()
                                .addGap(130, 130, 130)
                                .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jpnFundoLayout.createSequentialGroup()
                                        .addComponent(btnCancelar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGap(39, 39, 39)
                                        .addComponent(btnEditar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGap(423, 423, 423)
                                        .addComponent(btnFechar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(jpnFundoLayout.createSequentialGroup()
                                        .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jtxtfEmailAluno)
                                            .addComponent(jtxtfNomeAluno))
                                        .addGap(163, 163, 163))))
                            .addGroup(jpnFundoLayout.createSequentialGroup()
                                .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jlblNomeAluno)
                                    .addComponent(jlblTitulo)
                                    .addComponent(btnSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jpnFundoLayout.createSequentialGroup()
                                .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jlblDataAluguel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jlblTituloLivro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(18, 18, 18)
                                .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jpnFundoLayout.createSequentialGroup()
                                        .addComponent(jtxtfTituloLivro)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnAddLivro))
                                    .addGroup(jpnFundoLayout.createSequentialGroup()
                                        .addComponent(jformattedtxtfDataAluguel, javax.swing.GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE)
                                        .addGap(59, 59, 59)
                                        .addComponent(jlblDataDevolucao)
                                        .addGap(18, 18, 18)
                                        .addComponent(jformattedtxtfDataDevolucao, javax.swing.GroupLayout.DEFAULT_SIZE, 115, Short.MAX_VALUE)
                                        .addGap(41, 41, 41)))
                                .addGap(344, 344, 344)))
                        .addGap(38, 38, 38))
                    .addGroup(jpnFundoLayout.createSequentialGroup()
                        .addGap(130, 130, 130)
                        .addComponent(jtxtfTurma)
                        .addGap(487, 487, 487))
                    .addGroup(jpnFundoLayout.createSequentialGroup()
                        .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jlblTurma)
                            .addComponent(jlblEmailAluno))
                        .addGap(382, 382, 382))))
        );
        jpnFundoLayout.setVerticalGroup(
            jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpnBorda, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jpnFundoLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(jlblTitulo)
                .addGap(36, 36, 36)
                .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlblNomeAluno)
                    .addComponent(jtxtfNomeAluno))
                .addGap(27, 27, 27)
                .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlblEmailAluno)
                    .addComponent(jtxtfEmailAluno))
                .addGap(30, 30, 30)
                .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlblTurma)
                    .addComponent(jtxtfTurma))
                .addGap(32, 32, 32)
                .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpnFundoLayout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(jtxtfTituloLivro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jlblTituloLivro)
                        .addComponent(btnAddLivro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpnFundoLayout.createSequentialGroup()
                        .addGap(41, 41, 41)
                        .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jlblDataDevolucao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jformattedtxtfDataDevolucao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jpnFundoLayout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jlblDataAluguel, javax.swing.GroupLayout.DEFAULT_SIZE, 48, Short.MAX_VALUE)
                            .addComponent(jformattedtxtfDataAluguel))))
                .addGap(66, 66, 66)
                .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSalvar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnCancelar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnEditar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnFechar, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE))
                .addGap(67, 67, 67))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpnFundo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpnFundo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        setBounds(125, 50, 1043, 539);
    }// </editor-fold>//GEN-END:initComponents

    private void jtxtfNomeAlunoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtxtfNomeAlunoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtxtfNomeAlunoActionPerformed

    private void jtxtfTituloLivroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtxtfTituloLivroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtxtfTituloLivroActionPerformed

       //Eventos de Click dos botôes;
    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
     
    }//GEN-LAST:event_btnSalvarActionPerformed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnEditarActionPerformed

    private void btnFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFecharActionPerformed
      dispose();
    }//GEN-LAST:event_btnFecharActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
     dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    
    private void formMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_formMouseClicked

    private void jtxtfEmailAlunoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtxtfEmailAlunoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtxtfEmailAlunoActionPerformed

    private void btnAddLivroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddLivroActionPerformed

    }//GEN-LAST:event_btnAddLivroActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddLivro;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnFechar;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JFormattedTextField jformattedtxtfDataAluguel;
    private javax.swing.JFormattedTextField jformattedtxtfDataDevolucao;
    private javax.swing.JLabel jlblDataAluguel;
    private javax.swing.JLabel jlblDataDevolucao;
    private javax.swing.JLabel jlblEmailAluno;
    private javax.swing.JLabel jlblNomeAluno;
    private javax.swing.JLabel jlblTitulo;
    private javax.swing.JLabel jlblTituloLivro;
    private javax.swing.JLabel jlblTurma;
    private javax.swing.JPanel jpnBorda;
    private javax.swing.JPanel jpnFundo;
    private javax.swing.JTextField jtxtfEmailAluno;
    private javax.swing.JTextField jtxtfNomeAluno;
    private javax.swing.JTextField jtxtfTituloLivro;
    private javax.swing.JTextField jtxtfTurma;
    // End of variables declaration//GEN-END:variables
}
